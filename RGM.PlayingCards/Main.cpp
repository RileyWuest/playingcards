//App for Lab Exercise Two
//Part 1: Richard Maki
//Part 2: Riley Wuest
#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Suit { Heart, Spade, Club, Diamond };
enum Rank { Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace };

struct Card
{
	Suit suit;
	Rank rank;

};

void PrintCard(Card card) 
{
	
	Suit suit = Suit(Spade);
	Rank rank = Rank(Five);
	
	cout << "The " << suit << " of " << rank;
}

Card HighCard(Card card1, Card card2) 
{
	
	Rank rank1 = Rank(Six);
	Rank rank2 = Rank(Seven);

	if (rank1 > rank2) return card1;
	else if (rank1 == rank2) cout << "Its a tie! Double or Nothing! Pick again";
	else return card2;
}

int main()
{

	
	(void)_getch();
	return 0;

}
